# Differential LFP Simulation Library
Author: Vineet Tiruvadi


## Overview
New bidirectional deep brain stimulation (DBS) devices that use *differential* recording channels enable local field potentials (LFPs) alongside clinical therapy.
Even when they remove a significant part of the DBS, residual artifact can overwhelm downstream circuitry.
In this project I characterize _mismatch compression_ distortions and propose a mitigation strategy to enable more reliable oscillatory analyses of $\partial$LFP recordings for chronic DBS readouts.

![](imgs/dLFP_sim.png)

## Installation
### Dependencies
Oscillatory analyses depend on a separate library called DBSpace, available here:

## Notebook
A provided notebook enables interactive adjustment of the stimulation parameters.

## Methods
